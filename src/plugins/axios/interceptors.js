import router from '../router'
import progressCircular from '../../services/progress-circular'

export const requestInterceptor = {
  config: function (config) {    
    progressCircular.start()
    return config;
  },
  error: function (error) {
    progressCircular.finish()
    return Promise.reject(error);
  }
};

export const responseInterceptor = {
  success: function (response) {    
    progressCircular.finish()
    return response;
  },
  error: function (error) {      
    const status = error.response.status;
    if (status == 401) {
      window.location.reload();
    } else if (status == 403) {
      progressCircular.finish()
      router.replace('paginaNaoEncontrada');
    } else if (status == 409 || status == 404 || status == 400) {
      progressCircular.finish()
      // TODO
      // toastLauncher.launchToastErroGenerico(error.response.data.error);
      return Promise.reject(error);
    } else if (status == 422) {
      progressCircular.finish()
      // TODO
      // toastLauncher.launchToastFormularioInvalido();
      return Promise.reject(error);
    } else {
      progressCircular.finish()
      return Promise.reject(error);
    }
  }
};