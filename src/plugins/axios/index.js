import axios from 'axios'
import { requestInterceptor, responseInterceptor } from './interceptors'


const apiUrl = process.env.VUE_APP_API_URL
const apiPort = process.env.VUE_APP_API_PORT


axios.defaults.baseURL = `${apiUrl}:${apiPort}`;
axios.defaults.headers.get['Accept'] = 'application/json'
axios.defaults.headers.put['Content-Type'] = 'application/json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.request.use(
  requestInterceptor.config,
  requestInterceptor.error
)

axios.interceptors.response.use(
  responseInterceptor.success,
  responseInterceptor.error
)