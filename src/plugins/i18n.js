import Vue from 'vue';
import VueI18n from 'vue-i18n'
import { pt } from 'vuetify/lib/locale'
import store from './vuex'

Vue.use(VueI18n)

const messages = {
  'pt-BR': {
    $vuetify: pt,
    'Enviar Orçamento': 'Enviar Orçamento',
    'Usuário': 'Usuário',
    'Fechar': 'Fechar',
    'Salvar': 'Salvar',
    'Enviar Email': 'Enviar Email',
  }
}

export default new VueI18n({
  locale: store.state.locale,
  messages,
});