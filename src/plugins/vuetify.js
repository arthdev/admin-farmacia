import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import vuei18n from './i18n'
import colors from 'vuetify/lib/util/colors'
import '../assets/styles/font-roboto.css'

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdiSvg',
  },
  theme: {
    themes: {
      light: {
        primary: colors.indigo.base,
        secondary: colors.lime.darken1,
        accent: colors.lime.darken1,
      },
      dark: {
        primary: colors.indigo.lighten1,
        secondary: colors.lime.darken1,
        accent: colors.lime.darken1,
      },
    },
  },
  lang: {
    // aqui chegam apenas as chamadas onde key iniciar com "$vuetify.", do contrário o vuetify vai ignorar
    t: function (key, ...params) {
      return vuei18n.t(key, params)
    },
  }
});
