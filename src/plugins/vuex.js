import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: { name: 'Basílio', image: '' },
    expandDrawer: null,
    showProgressCircular: false,
    activeProgress: 0,
    locale: 'pt-BR',
    filiais: [],
  },
  mutations: {
    toggleDrawerVariant (state) {
      state.expandDrawer = !state.expandDrawer
    },
    showProgressCircular (state) {
      state.showProgressCircular = true
    },
    hideProgressCircular (state) {
      state.showProgressCircular = false
    },
    loadFiliais (state, filiais) {
      state.filiais = filiais
    },
  },
  getters: {
    locale (state) {
      return state.locale || 'pt-BR'
    }
  }
})