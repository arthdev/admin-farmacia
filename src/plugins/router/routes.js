import Login from '../../components/pages/Login.vue'
import Admin from '../../components/admin/layout/Admin.vue'
import EnviarOrcamento from '../../components/admin/pages/EnviarOrcamento.vue'

export default [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      name: 'Login',
    }
  },
  {
    path: '/',
    name: 'admin',
    component: Admin,
    meta: {
     name: 'Admin', 
    },
    children: [
      {
        path: 'enviar-orcamento',
        name: 'enviar_orcamento',
        component: EnviarOrcamento,
        meta: {
          name: 'Enviar Orçamento',
        }
      }
    ]
  },
]