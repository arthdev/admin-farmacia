import Vue from 'vue'
import App from './components/App.vue'
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n'
import store from './plugins/vuex'
import router from './plugins/router'
import './plugins/axios'
import moment from 'moment'
import farmaciaService from './services/farmacia-service'

Vue.config.productionTip = false

// login: ver portal do cliente ciasc
// store.commit('setUser', user)
farmaciaService.findFiliais()
  .then(r => {
    store.commit('loadFiliais', r)
  })
// linguagem global
moment.locale(store.getters.locale)

new Vue({
  vuetify,
  i18n,
  store,
  router,
  render: h => h(App),
}).$mount('#app')