import axios from 'axios'

export default {
  findOrcamentos (params) {
    return axios
      .get('/api/orcamentos', { params })
      .then(
        response => {
          return Promise.resolve(response.data)
        }, 
        error => {
          return Promise.reject(error);
        }
      )
  },
  findFiliais () {
    return axios
      .get('/api/filiais')
      .then(
        response => {
          return Promise.resolve(response.data)
        }, 
        error => {
          return Promise.reject(error);
        }
      )
  },
  getOrcamento (numero, filial) {
    return axios
      .get(`/api/orcamentos/${numero}/${filial}`)
      .then(
        response => {
          return Promise.resolve(response.data)
        }, 
        error => {
          return Promise.reject(error);
        }
      )
  },

  enviarEmail (orcamento) {
    return axios
      .post(
        '/api/enviar-email', 
        orcamento
      )
      .then(() => {
          return Promise.resolve()
        }, error => {
          return Promise.reject(error);
        }
      )
  }
}