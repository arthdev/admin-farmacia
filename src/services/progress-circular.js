import store from '../plugins/vuex'

export default (function () {
  let activeProgress = 0

  return {
    start () {
      activeProgress++
      store.commit('showProgressCircular')
    },
    finish () {
      activeProgress--
      if (activeProgress < 1) {
        store.commit('hideProgressCircular')
      }
    },
  }
})()