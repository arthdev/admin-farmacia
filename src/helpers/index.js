export function isNullOrUndefined (v) {
  return (typeof v === 'undefined' || v === null)
}