import { isNullOrUndefined } from '../helpers'

export default {
  convertFromApiFormat (decimal) {
    if (isNullOrUndefined(decimal)) {
      return decimal
    }

    return parseFloat(decimal.replace(',', ''))
  },
  convertFromClientFormat (decimal) {
    if (isNullOrUndefined(decimal)) {
      return decimal
    }

    return parseFloat(decimal.replace('.', '').replace(',', '.'))
  },
}