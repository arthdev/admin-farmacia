import { isNullOrUndefined } from '../helpers'

export default {
  formatApiDecimal (decimal, places = 2) {
    if (isNullOrUndefined(decimal)) {
      return decimal  
    }

    return new Intl.NumberFormat('pt-BR', { minimumFractionDigits: places }).format(parseFloat(decimal));
  },
}