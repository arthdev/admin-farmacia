import moment from 'moment'
import { SERVER_DATE_FORMAT } from '../constants/contants'

export default {
  formatApiDate (date) {
    return this.formatIso8061Date(date)
  },
  formatClientDateInApiDate (date) {
    return moment(date, 'DD/MM/YYYY', true).format(SERVER_DATE_FORMAT)
  },
  formatIso8061Date (date) {
    return moment(date, SERVER_DATE_FORMAT, true).format('DD/MM/YYYY')
  },
}